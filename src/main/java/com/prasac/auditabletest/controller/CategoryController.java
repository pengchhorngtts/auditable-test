package com.prasac.auditabletest.controller;

import com.prasac.auditabletest.model.dto.CategoryDTO;
import com.prasac.auditabletest.model.entity.CategoryEntity;
import com.prasac.auditabletest.repository.CategoryRepo;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.lang.reflect.Type;
import java.util.List;

/**
 * Created by Mut Pengchhorng
 * Date     : 6/2/20, 4:41 PM
 * Email    : pengchhorng.mut@prasac.com.kh
 */
@RestController
@RequestMapping(value = "categories")
public class CategoryController {

    private final CategoryRepo categoryRepo;
    private final ModelMapper modelMapper;

    public CategoryController(CategoryRepo categoryRepo, ModelMapper modelMapper) {
        this.categoryRepo = categoryRepo;
        this.modelMapper = modelMapper;
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<CategoryDTO> findByIdDTO (@PathVariable("id") Long id) {
        CategoryDTO category = modelMapper.map(categoryRepo.getOne(id), CategoryDTO.class);
        return new ResponseEntity<>(category, HttpStatus.OK);
    }

    @GetMapping
    public ResponseEntity<List<CategoryDTO>> findAllDTO () {
        Type dtoType = new TypeToken<List<CategoryDTO>>(){}.getType();
        List<CategoryDTO> categories = modelMapper.map(categoryRepo.findAll(), dtoType);
        return new ResponseEntity<>(categories, HttpStatus.OK);
    }

//    @GetMapping
//    public ResponseEntity<List<CategoryEntity>> findAllCategory() {
//        return new ResponseEntity<>(categoryRepo.findAll(), HttpStatus.OK);
//    }

    @PostMapping
    public ResponseEntity<CategoryDTO> create(@RequestBody CategoryDTO category) {
        CategoryEntity entity = modelMapper.map(category, CategoryEntity.class);
        return new ResponseEntity<>(modelMapper.map(categoryRepo.save(entity), CategoryDTO.class), HttpStatus.OK);
    }
//    @PostMapping
//    public ResponseEntity<CategoryEntity> create(@RequestBody CategoryDTO category) {
//        CategoryEntity entity = modelMapper.map(category, CategoryEntity.class);
//        return new ResponseEntity<>(categoryRepo.save(entity), HttpStatus.OK);
//    }
}
