package com.prasac.auditabletest.controller;

import com.prasac.auditabletest.model.entity.ProductEntity;
import com.prasac.auditabletest.model.dto.ProductDTO;
import com.prasac.auditabletest.repository.ProductRepo;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.lang.reflect.Type;
import java.util.List;

/**
 * Created by Mut Pengchhorng
 * Date     : 6/2/20, 11:30 AM
 * Email    : pengchhorng.mut@prasac.com.kh
 */
@RestController
@RequestMapping(value = "products")
public class ProductController {
    private final ProductRepo productRepo;
    private final ModelMapper modelMapper;

    public ProductController(ProductRepo productRepo, ModelMapper modelMapper) {
        this.productRepo = productRepo;
        this.modelMapper = modelMapper;
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<ProductDTO> findByIdDTO (@PathVariable("id") Long id) {
        ProductDTO product = modelMapper.map(productRepo.getOne(id), ProductDTO.class);
        return new ResponseEntity<>(product, HttpStatus.OK);
    }

    @GetMapping
    public ResponseEntity<List<ProductDTO>> findAllDTO () {
        Type dtoType = new TypeToken<List<ProductDTO>>(){}.getType();
        List<ProductDTO> products = modelMapper.map(productRepo.findAll(), dtoType);
        return new ResponseEntity<>(products, HttpStatus.OK);
    }

//    @GetMapping
//    public ResponseEntity<List<ProductEntity>> findAllProduct() {
//        return new ResponseEntity<>(productRepo.findAll(), HttpStatus.OK);
//    }

    @PostMapping
    public ResponseEntity<ProductDTO> create(@RequestBody ProductDTO product) {
        ProductEntity entity = modelMapper.map(product, ProductEntity.class);
        return new ResponseEntity<>(modelMapper.map(productRepo.save(entity),ProductDTO.class), HttpStatus.OK);
    }
//    @PostMapping
//    public ResponseEntity<ProductEntity> create(@RequestBody ProductDTO product) {
//        ProductEntity entity = modelMapper.map(product, ProductEntity.class);
//        return new ResponseEntity<>(productRepo.save(entity), HttpStatus.OK);
//    }
}
