package com.prasac.auditabletest.repository;

import com.prasac.auditabletest.model.entity.ProductEntity;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by Mut Pengchhorng
 * Date     : 6/2/20, 11:29 AM
 * Email    : pengchhorng.mut@prasac.com.kh
 */
public interface ProductRepo extends JpaRepository<ProductEntity, Long> {
}
