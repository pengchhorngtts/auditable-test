package com.prasac.auditabletest.repository;

import com.prasac.auditabletest.model.entity.CategoryEntity;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by Mut Pengchhorng
 * Date     : 6/2/20, 4:41 PM
 * Email    : pengchhorng.mut@prasac.com.kh
 */
public interface CategoryRepo extends JpaRepository<CategoryEntity, Long> {
}
