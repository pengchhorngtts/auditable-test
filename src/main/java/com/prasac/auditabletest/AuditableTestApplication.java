package com.prasac.auditabletest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AuditableTestApplication {

    public static void main(String[] args) {
        SpringApplication.run(AuditableTestApplication.class, args);
    }

}
