package com.prasac.auditabletest.model.dto;

import java.io.Serializable;
import java.util.List;

/**
 * Created By Mut Pengchhorng
 * Date    : 2020-02-06, 7:04 PM
 * E-mail  : pengchhorngtts@gmail.com
 */
public class CategoryDTO implements Serializable {
    private Long id;
    private String name;
    private String code;
    private List<CategoryProductDTO> products;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public List<CategoryProductDTO> getProducts() {
        return products;
    }

    public void setProducts(List<CategoryProductDTO> products) {
        this.products = products;
    }
}
