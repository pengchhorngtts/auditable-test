package com.prasac.auditabletest.model.dto;

import java.io.Serializable;

/**
 * Created by Mut Pengchhorng
 * Date     : 6/2/20, 11:37 AM
 * Email    : pengchhorng.mut@prasac.com.kh
 */
public class ProductDTO implements Serializable {

    private Long id;
    private String name;
    private String code;
    private ProductCategoryDTO category;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public ProductCategoryDTO getCategory() {
        return category;
    }

    public void setCategory(ProductCategoryDTO category) {
        this.category = category;
    }
}
