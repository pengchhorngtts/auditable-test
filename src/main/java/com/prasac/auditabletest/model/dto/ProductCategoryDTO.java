package com.prasac.auditabletest.model.dto;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Mut Pengchhorng
 * Date     : 6/2/20, 4:17 PM
 * Email    : pengchhorng.mut@prasac.com.kh
 */
public class ProductCategoryDTO implements Serializable {

    private Long id;
    private String name;
    private String code;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

}
