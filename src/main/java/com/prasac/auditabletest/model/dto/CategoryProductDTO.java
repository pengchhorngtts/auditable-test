package com.prasac.auditabletest.model.dto;

/**
 * Created By Mut Pengchhorng
 * Date    : 2020-02-06, 7:05 PM
 * E-mail  : pengchhorngtts@gmail.com
 */
public class CategoryProductDTO {
    private Long id;
    private String name;
    private String code;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
