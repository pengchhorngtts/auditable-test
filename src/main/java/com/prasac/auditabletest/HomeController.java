package com.prasac.auditabletest;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by Mut Pengchhorng
 * Date     : 6/2/20, 10:34 AM
 * Email    : pengchhorng.mut@prasac.com.kh
 */
@RestController
public class HomeController {
    @GetMapping
    public String home() {
        return "<h1>Hello world!</h1>";
    }
}
